from flask_assets import Bundle, Environment
from dia_web.diaApp import app

bundles = {

    'home_js': Bundle(
        'libs/js/vendor/jquery-2.2.4.min.js',
        'libs/moment/moment-2.18.1.js',
        'libs/js/vendor/popper.js',
        'libs/js/vendor/bootstrap.min.js',
        'libs/js/jquery.ajaxchimp.min.js',
        'libs/js/jquery.nice-select.min.js',
        'libs/js/jquery.sticky.js',
        'libs/js/nouislider.min.js',
        'libs/js/jquery.magnific-popup.min.js',
        'libs/js/owl.carousel.min.js',
        'libs/js/gmaps.min.js',
        'libs/ion.rangeSlider-master/js/ion.rangeSlider.js',
        'libs/js/parallax.min.js',
        'libs/datatables/dataTables-1.10.16/js/jquery.dataTables.js',
        'libs/blockui/jquery.blockUI.js',
        'libs/jplist-es6/jplist.min.js',
        'libs/jplist-es6/polyfill.min.js',
        'libs/products-comparison-table-master/js/modernizr.js',
        'libs/products-comparison-table-master/js/main.js',
        'libs/js/main.js',
        'project/js/main.js',
    ),

    'home_css': Bundle(
        'libs/css/linearicons.css',

        'libs/css/magnific-popup.css',
        'libs/css/availability-calendar.css',
        'libs/css/jquerysctipttop.css',

        'libs/css/owl.carousel.css',
        'libs/css/themify-icons.css',
        'libs/css/font-awesome.min.css',
        'libs/css/nice-select.css',
        'libs/css/nouislider.min.css',
        'libs/css/bootstrap.css',
        'libs/bootstrap-social/bootstrap-social.css',
        'libs/ion.rangeSlider-master/css/ion.rangeSlider.css',
        'libs/datatables/dataTables-1.10.16/css/jquery.dataTables.css',
        'libs/jplist-es6/jplist.styles.css',
        #'libs/products-comparison-table-master/css/reset.css',
        'libs/products-comparison-table-master/css/style.css',
        'libs/css/main.css',
        #'libs/ha/css/main.css',
        'project/css/style.css',

    ),

    'admin_js': Bundle(),

    'admin_css': Bundle()
}

assets = Environment(app)

assets.register(bundles)
