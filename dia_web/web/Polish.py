from dia_web.diaApp import db


class POLISH(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    polish = db.Column(db.String(30))
    polish_desc = db.Column(db.String(30))
    polish_cdesc = db.Column(db.String(30))
    ordering = db.Column(db.Numeric)

    def __repr__(self):
        return '<Polish %r>' % self.id
