from dia_web.diaApp import db


class CLARITY(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    clarity = db.Column(db.String(30))
    clarity_desc = db.Column(db.String(30))
    clarity_cdesc = db.Column(db.String(30))
    ordering = db.Column(db.Numeric)

    def __repr__(self):
        return '<Clarity %r>' % self.id
