import os
import json
import mysql.connector

from decimal import Decimal
from mysql.connector import Error
from flask import render_template, session, Blueprint, json, request, flash
from dia_web.diaApp import app, _basedir, db
from dia_web.publicEndpoint import public_endpoint
from dia_web.web.Cut import CUT
from dia_web.web.Diamond import DIAMOND
from dia_web.web.Color import COLOR
from dia_web.web.Symmetry import SYMMETRY
from dia_web.web.Polish import POLISH
from dia_web.web.Fluorescence import FLUORESCENCE
from dia_web.web.Clarity import CLARITY
from dia_web.web.switch import switch
from dia_web.web.DiamondDao import DiamondDao
from dia_web.web.ExtendJSONEncoder import ExtendJSONEncoder
from dia_web.web.AlchemyEncoder import AlchemyEncoder
from dia_web.config import Config

import requests
import time
import atexit
from apscheduler.schedulers.background import BackgroundScheduler
from datetime import datetime
from datetime import timedelta

logger = app.logger
web_blueprint = Blueprint('login_blueprint', __name__,
                        template_folder=os.path.join(_basedir, 'templates', 'web'),url_prefix='')

diamond_dao = DiamondDao()

def find_ranked_diamond():
    now = datetime.now() + timedelta(days=1)  # current date and time
    time = now.strftime("%Y%m%d")
    print("time:", time)
    diamond_dao.find_ranked_diamond(time)

cron = BackgroundScheduler(daemon=True)
#cron.add_job(find_ranked_diamond, trigger='interval', seconds=5)
cron.add_job(find_ranked_diamond, trigger='cron', hour='23', minute='0')
cron.start()
# Explicitly kick off the background thread

# Shutdown your cron thread if the web process is stopped
atexit.register(lambda: cron.shutdown(wait=False))

def track_event(category, action, label=None, value=0):
    data = {
        'v': '1',  # API Version.
        'tid': Config.GA_TRACKING_ID,  # Tracking ID / Property ID.
        # Anonymous Client Identifier. Ideally, this should be a UUID that
        # is associated with particular user, device, or browser instance.
        'cid': '555',
        't': 'event',  # Event hit type.
        'ec': category,  # Event category.
        'ea': action,  # Event action.
        'el': label,  # Event label.
        'ev': value,  # Event value, must be an integer
    }

    response = requests.post(
        'https://www.google-analytics.com/collect', data=data)

    # If the request fails, this will raise a RequestException. Depending
    # on your application's needs, this may be a non-error and can be caught
    # by the caller.
    response.raise_for_status()

@web_blueprint.route('/')
@public_endpoint
def home():
    #diamond_dao = DiamondDao()
    paramDict = {}
    paramDict['carat_start'] = '1'
    paramDict['carat_end'] = '1'
    products = diamond_dao.find_diamond(paramDict)
    error = ''
    return render_template('index.html', error=error, page="home", products=products)

@web_blueprint.route('/blog')
@public_endpoint
def blog():

    error = ''
    return render_template('blog.html', error=error, page="blog")

@web_blueprint.route('/cart')
@public_endpoint
def cart():

    error = ''
    return render_template('cart.html', error=error, page="cart")

@web_blueprint.route('/category')
@public_endpoint
def category():
    #products = DIAMOND.query.all()
    #diamond_dao = DiamondDao()
    paramDict = request.args.to_dict()
    paramDict['carat'] = '1;1'
    products = diamond_dao.find_diamond(paramDict)
    princess_moments_count = diamond_dao.get_diamond_count('Princess Moments')
    queen_moments_count = diamond_dao.get_diamond_count('Queen Moments')
    all_moments_count = diamond_dao.get_diamond_count('')
    error = ''
    return render_template('category.html', error=error , page="category" , products = products, princess_moments_count=princess_moments_count, queen_moments_count=queen_moments_count, all_moments_count=all_moments_count)

@web_blueprint.route('/confirmation')
@public_endpoint
def confirmation():

    error = ''
    return render_template('confirmation.html', error=error, page="confirmation")

@web_blueprint.route('/contact')
@public_endpoint
def contact():

    error = ''
    return render_template('contact.html', error=error, page="contact")

@web_blueprint.route('/about')
@public_endpoint
def about():

    error = ''
    return render_template('about.html', error=error, page="about")

@web_blueprint.route('/why-moments')
@public_endpoint
def why_moments():

    error = ''
    return render_template('why-moments.html', error=error, page="why-moments")

@web_blueprint.route('/single-blog')
@public_endpoint
def singleBlog():

    error = ''
    return render_template('single-blog.html', error=error, page="single-blog")

@web_blueprint.route('/clarity')
@public_endpoint
def clarity():
    error = ''
    return render_template('clarity.html', error=error, page="clarity")

@web_blueprint.route('/color')
@public_endpoint
def color():
    error = ''
    return render_template('color.html', error=error, page="color")

@web_blueprint.route('/carat')
@public_endpoint
def carat():
    error = ''
    return render_template('carat.html', error=error, page="carat")

@web_blueprint.route('/cut')
@public_endpoint
def cut():
    error = ''
    return render_template('cut.html', error=error, page="cut")

@web_blueprint.route('/diamond-4cs')
@public_endpoint
def diamond_4cs():
    error = ''
    return render_template('diamond-4cs.html', error=error, page="diamond-4cs")

@web_blueprint.route('/single-product')
@public_endpoint
def singleProduct():
    #diamond_dao = DiamondDao()
    paramDict = request.args.to_dict()
    products = diamond_dao.find_diamond(paramDict)
    diamond = products[0]
    paramDict = {}
    #paramDict['carat_start'] = '1'
    #paramDict['carat_end'] = '1'
    paramDict['category_name'] = 'Princess Moments'
    recommend_diamond_list = diamond_dao.find_recommended_diamond(paramDict,diamond)
    print(recommend_diamond_list)
    error = ''
    '''
    track_event(
        category='Product',
        action='View',
        label=str(diamond.carat) + ' ' + diamond.color + ' ' + diamond.clarity + ' ' + diamond.cut + ' ' + str(diamond.cert_no))
    '''
    return render_template('single-product.html', error=error, page="single-product", diamond=diamond, recommend_diamond_list=recommend_diamond_list)

@web_blueprint.route('/diamond-search')
@public_endpoint
def diamond_search():
    #diamond_dao = DiamondDao()
    paramDict = request.args.to_dict()
    #paramDict['carat_start'] = '1'
    #paramDict['carat_end'] = '1'
    print(paramDict)
    is_trigger_search = paramDict['is_trigger_search']
    if is_trigger_search == 'false':
        result = {
            'data': '',
            'recordsFiltered': 0,
            'recordsTotal': 0
        }
        return json.dumps(result, cls=ExtendJSONEncoder)

    products = diamond_dao.find_diamond(paramDict)
    result = {
        'data': products,
        'recordsFiltered': products.__len__(),
        'recordsTotal': products.__len__()
    }
    jsonStr = json.dumps(result, cls=ExtendJSONEncoder)
    return jsonStr

@web_blueprint.route('/clarity-options')
@public_endpoint
def clarity_options():
    result = CLARITY.query.filter(CLARITY.ordering < 10).all()
    jsonStr = json.dumps(result, cls=AlchemyEncoder)
    return jsonStr

@web_blueprint.route('/color-options')
@public_endpoint
def color_options():
    result = COLOR.query.filter(COLOR.color < 'K').all()
    jsonStr = json.dumps(result, cls=AlchemyEncoder)
    return jsonStr

@web_blueprint.route('/cut-options')
@public_endpoint
def cut_options():
    result = CUT.query.filter(CUT.ordering < 10).all()
    jsonStr = json.dumps(result, cls=AlchemyEncoder)
    return jsonStr

@web_blueprint.route('/polish-options')
@public_endpoint
def polish_options():
    result = POLISH.query.all()
    jsonStr = json.dumps(result, cls=AlchemyEncoder)
    return jsonStr

@web_blueprint.route('/symmetry-options')
@public_endpoint
def symmetry_options():
    result = SYMMETRY.query.all()
    jsonStr = json.dumps(result, cls=AlchemyEncoder)
    return jsonStr

@web_blueprint.route('/fluorescence-options')
@public_endpoint
def fluorescence_options():
    result = FLUORESCENCE.query.all()
    jsonStr = json.dumps(result, cls=AlchemyEncoder)
    return jsonStr


@web_blueprint.route('/diamond-compare')
@public_endpoint
def diamondCompare():
    #products = DIAMOND.query.all()
    #diamond_dao = DiamondDao()
    paramDict = request.args.to_dict()
    paramDict['carat'] = '1;1'
    products = diamond_dao.find_diamond(paramDict)
    error = ''
    return render_template('diamond-compare.html', error=error, page="diamond-compare" , products = products)

