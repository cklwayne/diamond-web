from datetime import datetime
from json import JSONEncoder
from functools import singledispatch
from decimal import Decimal

from dia_web.web.Clarity import CLARITY
from dia_web.web.DiamondVo import DiamondVo


@singledispatch
def convert(o):
    #return o.__dict__
    raise TypeError('can not convert type')

@convert.register(datetime)
def _(o):
    return o.strftime('%b %d %Y %H:%M:%S')

@convert.register(Decimal)
def _(o):
    return float(o)

@convert.register(DiamondVo)
def _(o):
    return o.__dict__

@convert.register(CLARITY)
def _(o):
    return o.__dict__

class ExtendJSONEncoder(JSONEncoder):
    def default(self, obj):
        try:
            return convert(obj)
        except TypeError:
            return super(ExtendJSONEncoder, self).default(obj)

