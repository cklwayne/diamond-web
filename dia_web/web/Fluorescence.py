from dia_web.diaApp import db


class FLUORESCENCE(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    fluorescence = db.Column(db.String(30))
    fluorescence_desc = db.Column(db.String(30))
    fluorescence_cdesc = db.Column(db.String(30))
    ordering = db.Column(db.Numeric)

    def __repr__(self):
        return '<Fluorescence %r>' % self.id
