import mysql.connector
from mysql.connector import Error
from dia_web.web.DiamondVo import DiamondVo
from functools import lru_cache

import numpy as np
import pandas as pd

import sklearn
from sklearn.neighbors import NearestNeighbors
from datetime import datetime
from threading import Thread

class DiamondDao():

    def __init__(self):
        now = datetime.now()  # current date and time
        time = now.strftime("%Y%m%d")
        print("time:", time)
        thread = Thread(target=self.find_ranked_diamond, args=(time,))
        thread.start()

    def find_diamond(self,paramDist):
        try:
            tuples = ()
            connection = mysql.connector.connect(host='localhost', database='dia_dev', user='root', password='')
            sql_Query = " select d.carat, d.color, d.clarity, d.cut, d.shape, d.polish, d.symmetry, d.fluorescence, d.lab, d.measure_length, d.measure_width, d.measure_depth, d.table_percent, d.crown_angle, d.crown_height_percent, d.pavilion_angle, d.pavilion_depth_precent "
            sql_Query += " , d.gridle_thick, d.culet, d.total_depth_percent, d.cert_no, d.unit_price, d.supplier_id, d.category_id, d.location, d.status"
            sql_Query += " , CASE d.clarity     WHEN 'FL' THEN rp.fl 	WHEN 'IF' THEN rp.ifs 	WHEN 'VVS1' THEN rp.vvs1 	WHEN 'VVS2' THEN rp.vvs2 	WHEN 'VS1' THEN rp.vs1 	WHEN 'VS2' THEN rp.vs2 	WHEN 'SI1' THEN rp.si1 	WHEN 'SI2' THEN rp.si2 	WHEN 'I1' THEN rp.i1     ELSE 0 END * 7.85 * 100 * d.carat  as market_price "
            sql_Query += " , cs.score as carat_score, cls.score as clarity_score, cuts.score as cut_score, ps.score as polish_score, ss.score as symmetry_score, colors.score as color_score "
            sql_Query += " , d.id, color.color_type, clarity.clarity_desc, clarity.clarity_type, cut.cut_desc, color.color_desc "
            sql_Query += " , (cuts.score + ps.score + ss.score)/3 as cutting_score, ((cs.score + cls.score + colors.score + (cuts.score + ps.score + ss.score)/3 ) / 4 ) as overall_score "
            sql_Query += " , cat.category_name "
            sql_Query += " , diamond_pricing.list_price "
            sql_Query += " from diamond d "
            sql_Query += " inner join rap_price_carat rpc on d.carat = rpc.carat "
            sql_Query += " inner join rap_price rp on rp.status = 'A' and d.color = rp.color and rp.carat_start = rpc.carat_start and rp.carat_end = rpc.carat_end "
            sql_Query += " inner join carat_score cs on d.carat = cs.carat "
            sql_Query += " left join clarity_score cls on d.clarity = cls.clarity "
            sql_Query += " inner join clarity clarity on d.clarity = clarity.clarity "
            sql_Query += " left join cut_score cuts on d.cut = cuts.cut "
            sql_Query += " left join cut cut on d.cut = cut.cut "
            sql_Query += " left join polish_score ps on d.polish = ps.polish "
            sql_Query += " left join polish polish on d.polish = polish.polish "
            sql_Query += " left join symmetry_score ss on d.symmetry = ss.symmetry "
            sql_Query += " left join symmetry symmetry on d.symmetry = symmetry.symmetry "
            sql_Query += " left join fluorescence fluorescence on d.fluorescence = fluorescence.fluorescence "
            sql_Query += " left join color_score colors on d.color = colors.color "
            sql_Query += " left join color color on d.color = color.color "
            sql_Query += " left join diamond_category cat on d.category_id = cat.id "
            sql_Query += " inner join diamond_pricing diamond_pricing on diamond_pricing.diamond_id = d.id and diamond_pricing.active_ind = 'Y' "
            sql_Query += " where 1 = 1 "
            #sql_Query += " and supplier = 'LAXMI Diamond' "
            if ('id' in paramDist and paramDist['id']):
                sql_Query += " and d.id = %s "
                tuples += (paramDist['id'],)
            if ('category_name' in paramDist and paramDist['category_name']):
                sql_Query += " and cat.category_name = %s "
                tuples += (paramDist['category_name'],)
            if ('carat_start' in paramDist and paramDist['carat_start']):
                sql_Query += " and d.carat >= %s "
                tuples += (paramDist['carat_start'],)

            if ('carat_end' in paramDist and paramDist['carat_end']):
                sql_Query += " and d.carat <= %s "
                tuples += (paramDist['carat_end'],)

            if ('carat' in paramDist and paramDist['carat']):
                sql_Query += " and d.carat >= %s and  d.carat <= %s "
                data = paramDist['carat'].split(";")
                tuples += (data[0],data[1])

            if ('clarity' in paramDist and paramDist['clarity']):
                sql_Query += " and clarity.ordering >= %s and clarity.ordering <= %s "
                data = paramDist['clarity'].split(";")
                tuples += (data[0],data[1])

            if ('color' in paramDist and paramDist['color']):
                sql_Query += " and color.ordering >= %s and color.ordering <= %s "
                data = paramDist['color'].split(";")
                tuples += (data[0],data[1])

            if ('carat' in paramDist and paramDist['carat']):
                sql_Query += " and d.carat >= %s and d.carat <= %s "
                data = paramDist['carat'].split(";")
                tuples += (data[0],data[1])

            if ('unit_price' in paramDist and paramDist['unit_price']):
                sql_Query += " and d.unit_price >= %s and d.unit_price <= %s "
                data = paramDist['unit_price'].split(";")
                tuples += (data[0], data[1])

            if ('cut' in paramDist and paramDist['cut']):
                sql_Query += " and cut.ordering >= %s and cut.ordering <= %s "
                data = paramDist['cut'].split(";")
                tuples += (data[0],data[1])

            if ('polish' in paramDist and paramDist['polish']):
                sql_Query += " and polish.ordering >= %s and polish.ordering <= %s "
                data = paramDist['polish'].split(";")
                tuples += (data[0],data[1])

            if ('symmetry' in paramDist and paramDist['symmetry']):
                sql_Query += " and symmetry.ordering >= %s and symmetry.ordering <= %s "
                data = paramDist['symmetry'].split(";")
                tuples += (data[0],data[1])

            if ('fluorescence' in paramDist and paramDist['fluorescence']):
                sql_Query += " and fluorescence.ordering >= %s and fluorescence.ordering <= %s "
                data = paramDist['fluorescence'].split(";")
                tuples += (data[0],data[1])

            if ('list_price' in paramDist and paramDist['list_price']):
                sql_Query += " and diamond_pricing.list_price >= %s and diamond_pricing.list_price <= %s "
                data = paramDist['list_price'].split(";")
                tuples += (data[0], data[1])

            if ('id_list' in paramDist and paramDist['id_list']):
                id_list = paramDist['id_list']
                format_strings = ','.join(['%s'] * len(id_list))
                print(format_strings)
                sql_Query += " and d.id IN (%s)" % format_strings
                tuples += paramDist['id_list']
                print(tuples)

            print(tuples)
            print(sql_Query)
            cursor = connection.cursor(buffered=True)
            cursor.execute(sql_Query, tuples)
            results = cursor.fetchall()
            diamond_vo_list = []
            for record in results:
                diamond_vo = DiamondVo()
                # selecting column value into varible
                diamond_vo.carat = record[0]
                diamond_vo.color = record[1]
                diamond_vo.clarity = record[2]
                diamond_vo.cut = record[3]
                diamond_vo.shape = record[4]
                diamond_vo.polish = record[5]
                diamond_vo.symmetry = record[6]
                diamond_vo.fluorescence = record[7]
                diamond_vo.lab = record[8]
                diamond_vo.measure_length = record[9]
                diamond_vo.measure_width = record[10]
                diamond_vo.measure_depth = record[11]
                diamond_vo.table_percent = record[12]
                diamond_vo.crown_angle = record[13]
                diamond_vo.crown_height_percent = record[14]
                diamond_vo.pavilion_angle = record[15]
                diamond_vo.pavilion_depth_precent = record[16]
                diamond_vo.gridle_thick = record[17]
                diamond_vo.culet = record[18]
                diamond_vo.total_depth_percent = record[19]
                diamond_vo.cert_no = record[20]
                diamond_vo.unit_price = record[21]
                diamond_vo.supplier_id = record[22]
                diamond_vo.category_id = record[23]
                diamond_vo.location = record[24]
                diamond_vo.status = record[25]
                diamond_vo.market_price = record[26]
                diamond_vo.carat_score = record[27]
                diamond_vo.clarity_score = record[28]
                diamond_vo.cut_score = record[29]
                diamond_vo.polish_score = record[30]
                diamond_vo.symmetry_score = record[31]
                diamond_vo.color_score = record[32]
                diamond_vo.id = record[33]
                diamond_vo.color_type = record[34]
                diamond_vo.clarity_desc = record[35]
                diamond_vo.clarity_type = record[36]
                diamond_vo.cut_desc = record[37]
                diamond_vo.color_desc = record[38]
                diamond_vo.cutting_score = record[39]
                diamond_vo.overall_score = record[40]
                diamond_vo.category_name = record[41]
                diamond_vo.list_price = record[42]
                diamond_vo_list.append(diamond_vo)
                #print("column value: ", diamond_vo.carat)
            return diamond_vo_list
        except mysql.connector.Error as error:
            print("Failed to get record from database: {}".format(error))
        finally:
            # closing database connection.
            if (connection.is_connected()):
                cursor.close()
                connection.close()
                print("MySQL connection is closed")

    def get_diamond_count(self,category_name):
        try:
            tuples = ()
            connection = mysql.connector.connect(host='localhost', database='dia_dev', user='root', password='')
            sql_Query = " select count(1) "
            sql_Query += " from diamond d "
            sql_Query += " left join diamond_category dc on d.category_id = dc.id "
            sql_Query += " where 1 = 1 "
            if (category_name):
                sql_Query += " and dc.category_name = %s "
                tuples += (category_name,)

            print(tuples)
            print(sql_Query)
            cursor = connection.cursor(buffered=True)
            cursor.execute(sql_Query, tuples)
            result = cursor.fetchone()
            print(result[0])
            return result[0]
        except mysql.connector.Error as error:
            print("Failed to get record from database: {}".format(error))
        finally:
            # closing database connection.
            if (connection.is_connected()):
                cursor.close()
                connection.close()
                print("MySQL connection is closed")

    def memoize(self,func):
        cache = dict()

        def memoized_func(*args):
            if args in cache:
                return cache[args]
            result = func(*args)
            cache[args] = result
            return result

        return memoized_func

    @lru_cache(maxsize=4)
    def find_ranked_diamond(self,sys_date):
        try:
            paramDist = {}
            paramDist['category_name'] = 'Princess Moments'
            tuples = ()
            connection = mysql.connector.connect(host='localhost', database='dia_dev', user='root', password='')
            sql_Query = " select d.carat, d.color, d.clarity, d.cut, d.shape, d.polish, d.symmetry, d.fluorescence, d.lab, d.measure_length, d.measure_width, d.measure_depth, d.table_percent, d.crown_angle, d.crown_height_percent, d.pavilion_angle, d.pavilion_depth_precent "
            sql_Query += " , d.gridle_thick, d.culet, d.total_depth_percent, d.cert_no, d.unit_price, d.supplier_id, d.category_id, d.location, d.status"
            sql_Query += " , CASE d.clarity     WHEN 'FL' THEN rp.fl 	WHEN 'IF' THEN rp.ifs 	WHEN 'VVS1' THEN rp.vvs1 	WHEN 'VVS2' THEN rp.vvs2 	WHEN 'VS1' THEN rp.vs1 	WHEN 'VS2' THEN rp.vs2 	WHEN 'SI1' THEN rp.si1 	WHEN 'SI2' THEN rp.si2 	WHEN 'I1' THEN rp.i1     ELSE 0 END * 7.85 * 100 * d.carat  as market_price "
            sql_Query += " , cs.score as carat_score, cls.score as clarity_score, cuts.score as cut_score, ps.score as polish_score, ss.score as symmetry_score, colors.score as color_score "
            sql_Query += " , d.id, color.color_type, clarity.clarity_desc, clarity.clarity_type, cut.cut_desc, color.color_desc "
            sql_Query += " , (cuts.score + ps.score + ss.score)/3 as cutting_score, ((cs.score + cls.score + colors.score + (cuts.score + ps.score + ss.score)/3 ) / 4 ) as overall_score "
            sql_Query += " , cat.category_name "
            sql_Query += " , diamond_pricing.list_price "
            sql_Query += " , price_score.score as price_score "
            sql_Query += " from diamond d "
            sql_Query += " inner join rap_price_carat rpc on d.carat = rpc.carat "
            sql_Query += " inner join rap_price rp on rp.status = 'A' and d.color = rp.color and rp.carat_start = rpc.carat_start and rp.carat_end = rpc.carat_end "
            sql_Query += " inner join carat_score cs on d.carat = cs.carat "
            sql_Query += " left join clarity_score cls on d.clarity = cls.clarity "
            sql_Query += " inner join clarity clarity on d.clarity = clarity.clarity "
            sql_Query += " inner join cut_score cuts on d.cut = cuts.cut "
            sql_Query += " left join cut cut on d.cut = cut.cut "
            sql_Query += " inner join polish_score ps on d.polish = ps.polish "
            sql_Query += " left join polish polish on d.polish = polish.polish "
            sql_Query += " inner join symmetry_score ss on d.symmetry = ss.symmetry "
            sql_Query += " left join symmetry symmetry on d.symmetry = symmetry.symmetry "
            sql_Query += " left join fluorescence fluorescence on d.fluorescence = fluorescence.fluorescence "
            sql_Query += " inner join color_score colors on d.color = colors.color "
            sql_Query += " left join color color on d.color = color.color "
            sql_Query += " left join diamond_category cat on d.category_id = cat.id "
            sql_Query += " inner join diamond_pricing diamond_pricing on diamond_pricing.diamond_id = d.id and diamond_pricing.active_ind = 'Y' "
            sql_Query += " inner join price_score price_score on diamond_pricing.list_price between price_score.price_start and price_score.price_end "
            sql_Query += " where 1 = 1 "
            '''
            if ('carat_start' in paramDist and paramDist['carat_start']):
                sql_Query += " and d.carat >= %s "
                tuples += (paramDist['carat_start'],)

            if ('carat_end' in paramDist and paramDist['carat_end']):
                sql_Query += " and d.carat <= %s "
                tuples += (paramDist['carat_end'],)

            if ('category_name' in paramDist and paramDist['category_name']):
                sql_Query += " and cat.category_name = %s "
                tuples += (paramDist['category_name'],)
            '''
            print(tuples)
            print(sql_Query)
            cursor = connection.cursor(buffered=True)
            cursor.execute(sql_Query, tuples)

            print('Put it all to a data frame')
            # Put it all to a data frame
            sql_data = pd.DataFrame(cursor.fetchall())
            sql_data.columns = cursor.column_names
            print('After Put it all to a data frame')

            # Show the data
            print(sql_data.head())
            return sql_data

        except mysql.connector.Error as error:
            print("Failed to get record from database: {}".format(error))
        finally:
            # closing database connection.
            if (connection.is_connected()):
                cursor.close()
                connection.close()
                print("MySQL connection is closed")

    def find_recommended_diamond(self,paramDist,diamond):
        now = datetime.now()  # current date and time
        time = now.strftime("%Y%m%d")
        print("time:", time)
        sql_data = self.find_ranked_diamond(time)

        if (sql_data is not None and sql_data.__len__() > 0):
            print(self.find_ranked_diamond.cache_info())

            price_score = self.find_price_score(diamond.list_price)
            if (price_score is not None):
                # t = [1.01, 4, 4, 4.5, 4.5, 4.5, 4.5, 30000]
                t = [diamond.carat, diamond.carat_score, diamond.clarity_score, diamond.cut_score, diamond.polish_score,
                     diamond.symmetry_score, diamond.color_score, price_score]

                # feature_matix = sql_data.iloc[:, [27,28,29,30,31,32,39]].values
                feature_matix = sql_data.iloc[:, [0, 27, 28, 29, 30, 31, 32, 43]].values

                # Recommendation is made based upon 2 similar cars
                knn = NearestNeighbors(n_neighbors=9).fit(feature_matix)

                # printing the recommendation
                print(knn.kneighbors([t]))

                id_list = ()
                #diamond_list = []
                for idx, val in enumerate(knn.kneighbors([t])[1][0]):
                    print(sql_data.iloc[val:val + 1, [0, 1, 2, 3, 5, 6, 42, 33]])
                    print(sql_data.at[val,'id'],)
                    id_list += (str(sql_data.at[val,'id']),)
                    #paramDict = {}
                    #paramDict['id'] = str(sql_data.at[val,'id'])
                    #diamond_list.append(self.find_diamond(paramDict))

                paramDict = {}
                paramDict['id_list'] = id_list
                diamond_list = self.find_diamond(paramDict)
                return diamond_list

    def find_price_score(self,list_price):
        try:
            connection = mysql.connector.connect(host='localhost', database='dia_dev', user='root', password='')
            sql_Query = " select score from price_score where 1 = 1 and %(list_price)s between price_start and price_end "
            print(list_price)
            print(sql_Query)
            cursor = connection.cursor(buffered=True)
            cursor.execute(sql_Query, { 'list_price': list_price })
            result = cursor.fetchone()
            print(result[0])
            return result[0]

        except mysql.connector.Error as error:
            print("Failed to get record from database: {}".format(error))
        finally:
            # closing database connection.
            if (connection.is_connected()):
                cursor.close()
                connection.close()
                print("MySQL connection is closed")