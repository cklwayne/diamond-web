from dia_web.diaApp import db


class DIAMOND(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    carat = db.Column(db.Numeric)
    color = db.Column(db.String(1))
    clarity = db.Column(db.String(30))
    cut = db.Column(db.String(30))
    shape = db.Column(db.String(30))
    polish = db.Column(db.String(30))
    symmetry = db.Column(db.String(30))
    fluorescence = db.Column(db.String(30))
    lab = db.Column(db.String(30))
    measure_length = db.Column(db.Numeric)
    measure_width = db.Column(db.Numeric)
    measure_depth = db.Column(db.Numeric)
    table_percent = db.Column(db.Numeric)
    crown_angle = db.Column(db.Numeric)
    crown_height_percent = db.Column(db.Numeric)
    pavilion_angle = db.Column(db.Numeric)
    pavilion_depth_precent = db.Column(db.Numeric)
    gridle_thick = db.Column(db.Numeric)
    culet = db.Column(db.String(30))
    total_depth_percent = db.Column(db.Numeric)
    cert_no = db.Column(db.String(30))
    unit_price = db.Column(db.Numeric)

    def __repr__(self):
        return '<Diamond %r>' % self.id
