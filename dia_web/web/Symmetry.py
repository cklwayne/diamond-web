from dia_web.diaApp import db


class SYMMETRY(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    symmetry = db.Column(db.String(30))
    symmetry_desc = db.Column(db.String(30))
    symmetry_cdesc = db.Column(db.String(30))
    ordering = db.Column(db.Numeric)

    def __repr__(self):
        return '<Symmetry %r>' % self.id
