from dia_web.diaApp import db


class CUT(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cut = db.Column(db.String(30))
    cut_desc = db.Column(db.String(30))
    cut_cdesc = db.Column(db.String(30))
    ordering = db.Column(db.Numeric)

    def __repr__(self):
        return '<Cut %r>' % self.id
