from dia_web.diaApp import db


class COLOR(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    color = db.Column(db.String(30))
    color_desc = db.Column(db.String(30))
    color_cdesc = db.Column(db.String(30))
    ordering = db.Column(db.Numeric)

    def __repr__(self):
        return '<Color %r>' % self.id
