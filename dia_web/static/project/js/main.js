jQuery.fn.dataTableExt.oApi.fnSetFilteringEnterPress = function(oSettings) {
	var _that = this;

	this.each(function(i) {
		$.fn.dataTableExt.iApiIndex = i;
		var anControl = $('input', _that.fnSettings().aanFeatures.f);

		anControl.unbind('keyup search input').bind(
				'keyup search input',
				function(e) {
					if (anControl.val().length == "" || anControl.val().length > 2) {
						_that.fnFilter(anControl.val());
					}
				});
		return this;
	});
	return this;
};

$(document).on('preXhr.dt', function (e, settings, data) {
	$(e.target).block({

		message: $('<img height="42" width="42" src="static/img/project/spinner.gif" />'),
		'css': {
           'border': '0',
           'padding': '0',
           'width': '100%',
           'height': '100%',
           'top': '0',
           'left': '0',
           backgroundColor: '#000',
           opacity: .5,
         color: '#fff'
         },
         'overlayCSS': {
           'backgroundColor': '#000',
           'opacity': 0.1,
           'cursor': 'wait'
         }
    });
});

$(document).on('xhr.dt', function (e, settings, json, xhr) {
    $(e.target).unblock();
});

$.extend( $.fn.dataTable.defaults, {
    "responsive": true,
    /*"scrollY":        "600px",
    "scrollCollapse": false,*/
    "scrollX": true,
    "columnDefs": [
        { className: "dt-body-center", "targets": "_all" }
    ],
    language: {
    	loadingRecords: '&nbsp;',
    	emptyTable: "No data available in table",
    	"processing": '',
    	search: "_INPUT_",
    	searchPlaceholder: 'Filter Results',
    	"lengthMenu":     "&nbsp;Show _MENU_ entries",
	}
} );


$.fn.dataTable.moment = function ( format, locale ) {
	console.log(format);
    var types = $.fn.dataTable.ext.type;

    // Add type detection
    types.detect.unshift( function ( d ) {
        return moment( d, format, locale, true ).isValid() ?
            'moment-'+format :
            null;
    } );

    // Add sorting method - use an integer for the sorting
    types.order[ 'moment-'+format+'-pre' ] = function ( d ) {
        return moment( d, format, locale, true ).unix();
    };
};

function format(str, col) {
    col = typeof col === 'object' ? col : Array.prototype.slice.call(arguments, 1);

    return str.replace(/\{\{|\}\}|\{(\w+)\}/g, function (m, n) {
        if (m == "{{") { return "{"; }
        if (m == "}}") { return "}"; }
        return col[n];
    });
};

$( document ).ready(function() {
    $(".panel-heading:has(button.close-panel-btn)").click(function() {
        console.log("clicked");
        if ($(this).parent().parent().find('div.panel-body').is(':hidden')) {
            	$(this).parent().parent().find('div.panel-body').show();
        	$(this).find("button.close-panel-btn").removeClass('fa-angle-down');
        	$(this).find("button.close-panel-btn").addClass('fa-angle-up');
        } else {
        	$(this).parent().parent().find('div.panel-body').hide();
        	$(this).find("button.close-panel-btn").removeClass('fa-angle-up');
        	$(this).find("button.close-panel-btn").addClass('fa-angle-down');
        }
    });

    $( ".modal.datatable-modal" ).on('show.bs.modal', function(){
		setTimeout(function(){
// 			$($.fn.dataTable.tables( true ) ).css('width', '100%');
	        $($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();

	        // for jquery.dataTable.js
// 	         $($.fn.dataTable.tables( {visible: true, api: true} )).DataTable().columns.adjust();
	    }, 350);
	});
    $.fn.dataTable.moment( 'DD-MM-YYYY' );
});

function formatCurrency(total) {
    var neg = false;
    if(total < 0) {
        neg = true;
        total = Math.abs(total);
    }
    return (neg ? "-$" : '$') + parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
}

function createFunctionWithTimeout(callback, opt_timeout) {
  var called = false;
  function fn() {
    if (!called) {
      called = true;
      callback();
    }
  }
  setTimeout(fn, opt_timeout || 1000);
  return fn;
}