import os
from flask import Flask,request, g
import sys
from flask_sqlalchemy import SQLAlchemy
from flask_httpbasicauth import HTTPBasicAuth

from flask import Flask, redirect, url_for, flash, render_template
from flask_login import login_required, logout_user
from .config import Config
from .models import db, login_manager
from .oauth import blueprint
#from .cli import create_db

_basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config.from_object('dia_web.config.Config')
#app.config["SQLALCHEMY_DATABASE_URI"] = 'mysql+pymysql://root:P@ssw0rd@52.221.234.94/DIA_DEV'
app.config["SQLALCHEMY_DATABASE_URI"] = 'mysql+pymysql://root:@localhost/DIA_DEV'

app.config.from_object(Config)
app.register_blueprint(blueprint, url_prefix="/login")
#app.cli.add_command(create_db)
#db.init_app(app)
login_manager.init_app(app)

db = SQLAlchemy(app)
auth = HTTPBasicAuth()

from dia_web.web.controller import web_blueprint
app.register_blueprint(web_blueprint)

@app.route("/logout")
@login_required
def logout():
    logout_user()
    flash("You have logged out")
    return redirect(url_for("index"))


@app.route("/login")
def index():
    return render_template("login.html", page="login")


#@app.before_request
#def before_request():
    #if (request.endpoint and
    #        'static' not in request.endpoint and
    #        not getattr(app.view_functions[request.endpoint], 'is_public', False)):
    #else:
    #    print('is not public function', file=sys.stdout)
#
#
#    print('before request started', file=sys.stdout)
