import os

class Config(object):
    DEBUG = True,
    SECRET_KEY = 'secret_key123123',
    DB_USERNAME = "root",
    DB_PASSWORD = "root",

    SQLALCHEMY_TRACK_MODIFICATIONS = False

    SECRET_KEY = os.environ.get('SECRET_KEY') or 'supersekrit'
    # SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///app.sqlite3'
    # SQLALCHEMY_TRACK_MODIFICATIONS = False
    FACEBOOK_OAUTH_CLIENT_ID = os.environ.get('FACEBOOK_OAUTH_CLIENT_ID')
    FACEBOOK_OAUTH_CLIENT_SECRET = os.environ.get('FACEBOOK_OAUTH_CLIENT_SECRET')

    GA_TRACKING_ID = os.environ.get('GA_TRACKING_ID')

class ProductionConfig(Config):
    DEBUG = False,
    SECRET_KEY = 'secret_key123123'
